import React from 'react';
import ReactDom from 'react-dom';
import Index from './Index';
const App = () => <Index />;
ReactDom.render(<App />, document.getElementById('root') as HTMLElement);
