import React, { ReactElement } from 'react';
import './style.scss';
import Header from 'components/Header';

function Index(): ReactElement {
  return (
    <div>
      <Header />
      <span className="hello">Hello world!</span>
    </div>
  );
}

export default Index;
